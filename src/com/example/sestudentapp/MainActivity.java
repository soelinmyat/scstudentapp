package com.example.sestudentapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements OnClickListener, OnLoginCompleted {
	
	TextView messageView;
	EditText usernameInput;
	EditText passwordInput;
	Button submitButton;
	
	public static final String MYPREFERENCES = "MyPref";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String PASSKEY = "passkey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        usernameInput = (EditText)findViewById(R.id.editText1);
        passwordInput = (EditText)findViewById(R.id.editText2);
        submitButton = (Button)findViewById(R.id.button1);
        messageView = (TextView)findViewById(R.id.textView1);
        
        SharedPreferences preferences = getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE);
		String username = preferences.getString(USERNAME, "");
		String password = preferences.getString(PASSWORD, "");
		
		if(username.equals("") == false && password.equals("") == false) {
			usernameInput.setText(username);
			passwordInput.setText(password);
		}
        
        submitButton.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onClick(View v) {
		messageView.setText("");
		
		String username = usernameInput.getText().toString();
		String password = passwordInput.getText().toString();
		
		if(username.length() == 0 && password.length() == 0) {
			messageView.append("Please enter user credentials.\n");
		}
		
		else {		
			new MyAsyncTask(this).execute(username, password);
		}
		
	}
	
	private class MyAsyncTask extends AsyncTask<String, Integer, Boolean>{
		 
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://shielded-reef-1097.herokuapp.com/users/check_info/");
		private OnLoginCompleted listener;
		
		public MyAsyncTask(OnLoginCompleted listener) {
			this.listener = listener;
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			Boolean success = false;
			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair("username", params[0]));
				nameValuePairs.add(new BasicNameValuePair("password", params[1]));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				//InputStream is = entity.getContent();
				//responseText = convertStreamToString(is);
				String responseText = EntityUtils.toString(entity);
				JSONObject jsonObject = new JSONObject(responseText);
				String responseMsg = jsonObject.getString("message");
				if(responseMsg.equals("success")) {
					success = true;
					String responsePasskey = jsonObject.getString("passkey");
					SharedPreferences preferences = getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = preferences.edit();
					editor.putString(USERNAME, params[0]);
					editor.putString(PASSWORD, params[1]);
					editor.putString(PASSKEY, responsePasskey);
					editor.commit();
				}
				
				//responseText = String.valueOf(response.getStatusLine().getStatusCode());
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return success;
		}
 
		protected void onPostExecute(Boolean success){
			this.listener.onLoginCompleted(success);
		}
	}

	@Override
	public void onLoginCompleted(Boolean success) {
		if(success) {
			Intent intent = new Intent(this, RoutineActivity.class);
	    	startActivity(intent);
		}
		else {
			messageView.setText("The login credentials are wrong.");
		}
	}
}

package com.example.sestudentapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RoutineActivity extends ActionBarActivity implements OnClickListener, OnTaskFinished {

	private TextView logView; 
	private Button refreshButton;
	
	public static final String MYPREFERENCES = "MyPref";
	public static final String USERNAME = "username";
	public static final String PASSKEY = "passkey";
	public static final String SEHUB = "SEHUB";
	
	public BluetoothAdapter bluetoothAdapter;
	private CustomReceiver bluetoothReceiver;
	public List<String> IPs;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.routine_activity);
		
		IPs = new ArrayList<String>();
		
		logView = (TextView)findViewById(R.id.textView2);
		refreshButton = (Button)findViewById(R.id.button1);
		refreshButton.setOnClickListener(this);
		
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		bluetoothReceiver = new CustomReceiver(this);
		registerReceiver(bluetoothReceiver, filter);
		
		run_routine();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.routine_activity, menu);
		return true;
	}
	
	public void onClick(View v) {
		IPs.clear();
		run_routine();
	}
	
	private void run_routine() {
		bluetoothAdapter.startDiscovery();
	}
	 
	public class CustomReceiver extends BroadcastReceiver {
		
		public OnTaskFinished listener;
		
		public CustomReceiver(OnTaskFinished listener) {
			this.listener = listener;
		}
        
		@Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String name = device.getName();
                if(name != null && name.toLowerCase().contains(SEHUB.toLowerCase())) {
                	String[] temp = name.split("---");
                	if(temp.length > 1) {
                		logView.append("Found " + temp[1] + "\n");
                		String ip = temp[1];
                		IPs.add(ip);
                	}
                }
            }
            else { 
            	if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                   	logView.append(IPs.size() + " SEHubs found.\n");
                   	this.listener.onTaskFinished(true);
            	}
            	else {
            		if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
                    	logView.append("Searching for nearby SEHubs..\n");
                    }
            	}
            }
        }
    };

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(bluetoothAdapter != null) {
			bluetoothAdapter.cancelDiscovery();
		}
		unregisterReceiver(bluetoothReceiver);
	}

	@Override
	public void onTaskFinished(Boolean success) {
		SharedPreferences preferences = getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE);
		String username = preferences.getString("username", "");
		String passkey = preferences.getString("passkey", "");
		
		Iterator<String> iter = IPs.iterator();
		while(iter.hasNext()) {
			String currentIp = iter.next();
			logView.append(currentIp+"\n");
			new AnotherAsyncTask().execute(username, passkey, currentIp+"attendance/take/");
		}
	}
	
	private class AnotherAsyncTask extends AsyncTask<String, Integer, String>{
		 
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost;
		//= new HttpPost("http://172.20.10.6:8000/users/check_info/");
		//private OnLoginCompleted listener;
		
		//public MyAsyncTask(OnLoginCompleted listener) {
		//	this.listener = listener;
		//}
		
		@Override
		protected String doInBackground(String... params) {
			String responseMsg = "";
			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair("username", params[0]));
				nameValuePairs.add(new BasicNameValuePair("passkey", params[1]));
				httppost = new HttpPost(params[2]);
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				responseMsg = EntityUtils.toString(entity);
				JSONObject jsonObject = new JSONObject(responseMsg);
				responseMsg = jsonObject.getString("message");
				
				//responseText = String.valueOf(response.getStatusLine().getStatusCode());
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return responseMsg;
		}
 
		protected void onPostExecute(String message){
			logView.append(message+"\n");
		}
	}
}

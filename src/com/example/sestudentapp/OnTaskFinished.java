package com.example.sestudentapp;

public interface OnTaskFinished {
	void onTaskFinished(Boolean success);
}
